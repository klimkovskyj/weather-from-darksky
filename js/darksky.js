jQuery(document).ready(function($){
	$("section.forecast").click(function() {
		$(this).slideUp(600, function() {
			$(this).siblings("section").slideDown(600);
		});
	});
	$("section.forecast-mini").click(function() {
		$(this).slideUp(600, function() {
			$(this).siblings("section").slideDown(600);
		});
	});
	
	$("#minus  > span.m-box__icon-plus").click(function() {
		$("#minus  > span.m-box__icon-plus").css('display', 'none'),
		$("#minus  > span.m-box__icon-minus").css('display', 'block'),
		$("#more").slideDown(600);
	});
	$("#minus  > span.m-box__icon-minus").click(function() {
		$("#minus  > span.m-box__icon-minus").css('display', 'none'),
		$("#minus  > span.m-box__icon-plus").css('display', 'block'),
		$("#more").slideUp(600);
	});
	
	$("#minus  > p.fs-14").click(function() {
		var elemMore = $('#more');
		if (elemMore.is(':hidden')) {		
			$("#minus  > span.m-box__icon-plus").css('display', 'none'),
			$("#minus  > span.m-box__icon-minus").css('display', 'block'),
			$("#more").slideDown(600);
		} else {
			$("#minus  > span.m-box__icon-minus").css('display', 'none'),
			$("#minus  > span.m-box__icon-plus").css('display', 'block'),
			$("#more").slideUp(600);
		}
	});	
});
