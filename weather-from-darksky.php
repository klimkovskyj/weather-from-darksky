<?php if( !defined('WPINC') ) die; // If this file is called directly, abort
/**
 * Plugin Name: Weather from darksky
 * Description: A wordpress plugin to display weather from darksky.net
 * Version: 0.1
 * Author: Ivanchenko Ivan
 * Author URI: ivanchenko.v6@gmail.com
 */

// Plugin DIR:
if( !defined('WP_DARKSKY_DIR_PATH') ) {
    define('WP_DARKSKY_DIR_PATH', plugin_dir_path( __FILE__ ));
}

// Plugin URL:
if( !defined('WP_DARKSKY_DIR_URL') ) {
    define('WP_DARKSKY_DIR_URL', plugin_dir_url( __FILE__ ));
}

add_filter( 'the_content', function($content) {
    if ( has_shortcode($content, 'darksky_shortcode') )
    {
//add_action(	'wp_enqueue_scripts', function() {	
		wp_enqueue_style( 'dark-output-style', WP_DARKSKY_DIR_URL .'css/dark-output.min.css');
		wp_enqueue_style( 'darksky-style', WP_DARKSKY_DIR_URL .'css/darksky.css');
		wp_enqueue_script( 'darksky-style', WP_DARKSKY_DIR_URL .'js/darksky.js', array('jquery'));
    //});
    }
    return $content;
});

function get_icon($icon_name, $arr_icons) {
	if( array_key_exists(esc_attr($icon_name), $arr_icons) ) {
		return $arr_icons[esc_attr($icon_name)];
	}
}

function get_pressure_hPa($humidity) {
	$res = $humidity*0.7500637554192;
	return round($res);
}

function get_moon_fase($fase) {
	switch(true) {
		case ($fase < 0 && $fase >= 0.98): return 'm-box__icon-phase_0'; break;
		case ($fase < 0.06 && $fase >= 0): return 'm-box__icon-phase_0'; break;
		case ($fase < 0.12 && $fase >= 0.06): return 'm-box__icon-phase_1'; break;
		
		case ($fase < 0.19  && $fase >= 0.12): return 'm-box__icon-phase_3'; break;
		case ($fase < 0.26 && $fase >= 0.19): return 'm-box__icon-phase_4'; break;
		case ($fase < 0.33 && $fase >= 0.26): return 'm-box__icon-phase_5'; break;
		case ($fase < 0.40 && $fase >= 0.33): return 'm-box__icon-phase_6'; break;
		case ($fase < 0.46 && $fase >= 0.40): return 'm-box__icon-phase_7'; break;
		case ($fase < 0.54 && $fase >= 0.46): return 'm-box__icon-phase_8'; break;
		case ($fase < 0.60 && $fase >= 0.54): return 'm-box__icon-phase_9'; break;
		case ($fase < 0.67 && $fase >= 0.60): return 'm-box__icon-phase_10'; break;
		case ($fase < 0.73 && $fase >= 0.67): return 'm-box__icon-phase_11'; break;
		case ($fase < 0.80 && $fase >= 0.73): return 'm-box__icon-phase_12'; break;
		case ($fase < 0.86 && $fase >= 0.80): return 'm-box__icon-phase_13'; break;
		case ($fase < 0.92 && $fase >= 0.86): return 'm-box__icon-phase_14'; break;
		case ($fase < 0.98 && $fase >= 0.92): return 'm-box__icon-phase_15'; break;
		
		default: return 'm-box__icon-phase_8'; break;
	}
	
}

function get_wind_direction($direction) {	
	switch(true) {
		case ($direction < 22 && $direction >= 0): return 'пн'; break;
		case ($direction <= 360 && $direction > 338): return 'пн'; break;
		
		case ($direction < 68 && $direction >= 22): return 'пн-с'; break;
		case ($direction < 112 && $direction >= 68): return 'с'; break;
		case ($direction < 158 && $direction >= 112): return 'пд-с'; break;
		case ($direction < 202 && $direction >= 158): return 'пд'; break;
		case ($direction < 248 && $direction >= 202): return 'пд-з'; break;
		case ($direction < 292 && $direction >= 248): return 'з'; break;
		case ($direction < 338 && $direction >= 292): return 'пн-з'; break;
		
		default: return 'пн'; break;
	}
}

function get_wind_point($direction) {	
	switch(true) {
		case ($direction < 22 && $direction >= 0): return 'm-box__icon-wind m-box__icon-wind_north'; break;
		case ($direction <= 360 && $direction > 338): return 'm-box__icon-wind m-box__icon-wind_north'; break;
		
		case ($direction < 68 && $direction >= 22): return 'm-box__icon-wind m-box__icon-wind_north_east'; break;
		case ($direction < 112 && $direction >= 68): return 'm-box__icon-wind m-box__icon-wind_east'; break;
		case ($direction < 158 && $direction >= 112): return 'm-box__icon-wind m-box__icon-wind_south_east'; break;
		case ($direction < 202 && $direction >= 158): return 'm-box__icon-wind'; break;
		case ($direction < 248 && $direction >= 202): return 'm-box__icon-wind m-box__icon-wind_south_west'; break;
		case ($direction < 292 && $direction >= 248): return 'm-box__icon-wind m-box__icon-wind_west'; break;
		case ($direction < 338 && $direction >= 292): return 'm-box__icon-wind m-box__icon-wind_north_west'; break;
		
		default: return 'm-box__icon-wind'; break;
	}
}

function get_weekend_color($date_day) {	
	$weekend = date('w', $date_day);
	if (0 == $weekend || $weekend == 6) { return 'weekend-color'; }
}

function get_last_hours($last_hour) {	
	$current_hour = date('H');
	if ($current_hour > $last_hour) { return 'opacity-color'; }
}

function get_temperature_plus($temperature) {
	$res_temperature = round($temperature);
	if ($res_temperature == 0) { return abs($res_temperature); } elseif ($res_temperature > 0) { return '+'.$res_temperature; } else { return $res_temperature; }
}

/**
 * Display the shortcode
 */

add_shortcode('darksky_shortcode', 'display_darksky_shortcode');

function display_darksky_shortcode() {
	$week = array('неділя', 'понеділок', 'вівторок', 'середа', 'четвер',
	'п\'ятниця', 'субота');
	
	$week_short = array('нд', 'пн', 'вт', 'ср', 'чт', 'пт', 'сб');

	$month = array('', 'січня', 'лютого', 'березня', 'квітня', 'травня', 'червня',
	'липня', 'серпня', 'вересня', 'жовтня', 'листопада', 'грудня');
	
	$arr_icons = array(
		'clear-day' 		=> 'sunshine_none_day',
		'clear-night' 		=> 'sunshine_none_night',
		'rain'			=> 'mostly_cloudy_light_rain_day',
		'snow'			=> 'mostly_cloudy_light_snow_day',
		'sleet'			=> 'cloudy_light_snow_day',
		'strong-wind'		=> 'cloudy_none_day',
		'fog'			=> 'mostly_cloudy_none_day',
		'cloudy'		=> 'cloudy_none_day',
		'partly-cloudy-day'	=> 'sunshine_light_rain_day',
		'partly-cloudy-night'	=> 'sunshine_light_rain_night',
		'hail'			=> 'cloudy_thunderstorm_day',
		'thunderstorm'		=> 'mostly_cloudy_thunderstorm_day',
		'tornado'		=> 'cloudy_thunderstorm_day',
		'wind'			=> 'mostly_cloudy_none_day',
	);
	date_default_timezone_set('Europe/Kiev');	
	$thisday = time();	
	//$request_url2 = 'https://api.darksky.net/forecast/9a04d5703780332dadaa95ec276334a6/49.4696,24.1386,'.$thisday.'?lang=uk&units=si&exclude=flags';
	//$response2 = wp_remote_get( esc_url_raw($request_url2), array('timeout' => 120, 'httpversion' => '1.1'));
	//$currentlyday = json_decode( wp_remote_retrieve_body( $response2 ), true );
	
	//$request_url = 'https://api.darksky.net/forecast/9a04d5703780332dadaa95ec276334a6/49.4696,24.1386?lang=uk&units=si&extend=hourly&exclude=flags';
	//$response = wp_remote_get( esc_url_raw($request_url), array('timeout' => 120, 'httpversion' => '1.1'));
	//$darksky = json_decode( wp_remote_retrieve_body( $response ), true );
	
	$darksky = json_decode(file_get_contents('/home/loner/Завантаження/Робота_WordPress/Дужич-погода/49.4696,24.1386.json'), true);
	$currentlyday = json_decode(file_get_contents('/home/loner/Завантаження/Робота_WordPress/Дужич-погода/49.4696,24.1386,1522183217-day.json'), true);
	
	ob_start();
	$hourly_data = isset($darksky['hourly']['data']) ? $darksky['hourly']['data'] : false;
	$current_data = isset($currentlyday['hourly']['data']) ? $currentlyday['hourly']['data'] : false;
	//get a begin next day
	if($hourly_data) {
		$date_format = 'n/j/Y';
		$time_now = date($date_format);//, current_time('timestamp', 1));
		foreach( $hourly_data as $key => $day ) {
			if(isset($day['time']) && $time_now != date($date_format, $day['time'])) {
				$time_next_day = $key;
				break;
			}
		}
	}
	//echo $time_next_day;
	include(WP_DARKSKY_DIR_PATH . '/templates/darksky-template.php');
    
    $output = ob_get_contents();
    ob_end_clean();
    return $output;	
}
