<?php if( !defined('WPINC') ) die;
/**
 * Template of darksky shortcode
 **/
?>

<?php
$sunriseTime = absint($currentlyday['daily']['data'][0]['sunriseTime']);
$sunsetTime = absint($currentlyday['daily']['data'][0]['sunsetTime']);
$long_res = $sunsetTime - $sunriseTime - 10800;

$long_day = date('H год i хв', $long_res);
?>

<div class="container-fluide">
	
	<div class="today pb-1 mb-2">		
		<div class=" pt-2 pb-2">
			<h2 class="city-name text-center" style="font-weight:normal;">Погода в м.Новий Розділ</h2>
			<hr style="margin-bottom:0rem;margin-top:0rem;border-top-color:#dfdfdf;">
			<div class="text-center">
				<span class="m-box__icon-big <?php echo get_icon($darksky['currently']['icon'], $arr_icons); ?> "></span>				
				<span class="ml-1" style="font-size:50px;vertical-align:middle;"><?php echo get_temperature_plus($darksky['currently']['temperature']); ?>&deg;</span>
				<span class="m-box__icon-arrow"></span>
			</div>
				<div class="text-center fs-14"><?php echo esc_attr($darksky['currently']['summary']); ?></div>
				<div class="text-center fs-14">Відчувається як <?php echo get_temperature_plus($darksky['currently']['apparentTemperature']); ?>&deg;</div>
			</div>
				
		<div class="row text-center no-gutters mt-1">
			<div class="col">
				<span class="m-box__icon <?php echo get_wind_point($darksky['currently']['windBearing']); ?>"></span>
				<p class="fs-14"><?php echo get_wind_direction($darksky['currently']['windBearing']); ?>,
				<?php echo round($darksky['currently']['windSpeed']); ?> м/с</p>
			</div>
			<div class="col">
				<span class="m-box__icon m-box__icon-pressure"></span>
				<p class="fs-14"><?php echo get_pressure_hPa($darksky['currently']['pressure']); ?> мм</p>
			</div>
			<div class="col">
				<span class="m-box__icon m-box__icon-humidity"></span>
				<p class="fs-14"><?php echo absint($darksky['currently']['humidity']*100); ?>%</p>
			</div>
			<div id="minus" class="col">
				<span style="cursor:pointer;" class="m-box__icon m-box__icon-plus"></span>
				<span style="cursor:pointer;" class="m-box__icon m-box__icon-minus"></span>
				<p style="cursor:pointer;" class="fs-14">Іще</p>
			</div>
		</div>
		
		<div id="more"  style="display:none">
			<div class="row fs-14 mb-4 mt-4">
				<div class="col text-right">
					<ul class="list-inline " style="">
						<li class="list-inline-item text-left">Схід / захід<br>Тривалість дня</li>
					</ul>
				</div>
				<div class="col text-left">
				  <span><?php echo date('H:i', $sunriseTime); ?> - <?php echo date('H:i', $sunsetTime); ?></span><br>
				  <span><?php echo $long_day; ?></span>
				</div>
			</div>
			<p class="text-center fs-12">Дані pogoda.ngs.ru</p>
		</div>
  
	</div><!--.row.today/-->
	
<div class="all-day">
<article>
	<section class="forecast">	
		<div class="row pl-2 no-gutters">
			<div class="col pt-2 fs-14 <?php echo get_weekend_color(time()); ?>">
				<span class="">Сьогодні
				<?php echo (int)date('d');?> 
				<?php echo $month[(int)date('n')];?><br>
				<?php echo $week[(int)date('w')];?></span>
			</div>
			<div class="col text-right pt-2">
				<ul class="list-inline" style="margin-bottom:-1rem; margin-left:-20px !important;">
				  <li class="list-inline-item">
					  <i class="m-box__icon <?php echo get_moon_fase($currentlyday['daily']['data'][0]['moonPhase']); ?> mr-0 "  style="margin-bottom:-2px !important;"></i></li>
				  <li class="list-inline-item mr-0 mt-1 text-left fs-12"><span>Схід: <?php echo date('H:i', $sunriseTime);?><br>
				  Захід: <?php echo date('H:i', $sunsetTime);?></span></li>
				  <li class="list-inline-item "><i class="m-box__icon-open m-box__icon-open_no-pos m-box__icon-open_up mr-2 ml-0" style="margin-top:-1rem;"></i></li>
				</ul>
			</div>
		</div><!--.row/-->
		<hr style="margin-bottom:0.5rem;margin-top:0.5rem;border-top-color:#cacaca;">
		<div class="row text-center no-gutters">
			<div class="col fs-12 <?php echo get_last_hours(6); ?>">
				<ul class="list-unstyled">					
					<li class="fs-14">Ніч</li>			
					<li><span class="m-box__icon <?php echo get_icon($current_data[3]['icon'], $arr_icons); ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo get_temperature_plus($current_data[3]['temperature']); ?></span></li>
					<li><?php echo round($current_data[3]['windSpeed']); ?> м/с, <?php echo get_wind_direction($current_data[3]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($current_data[3]['pressure']); ?> мм</li>
					<li><?php echo absint($current_data[3]['humidity']*100); ?>%</li>
				</ul>
			</div>
			<div class="col fs-12 <?php echo get_last_hours(10); ?>">
				<ul class="list-unstyled">					
					<li class="fs-14">Ранок</li>
					<li><span class="m-box__icon <?php echo get_icon($current_data[8]['icon'], $arr_icons); ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo get_temperature_plus($current_data[8]['temperature']); ?></span></li>
					<li><?php echo round($current_data[8]['windSpeed']); ?> м/с, <?php echo get_wind_direction($current_data[8]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($current_data[8]['pressure']); ?> мм</li>
					<li><?php echo absint($current_data[8]['humidity']*100); ?>%</li>
				</ul>
			</div>
			<div class="col fs-12 <?php echo get_last_hours(20); ?>">
				<ul class="list-unstyled">					
					<li class="fs-14">День</li>
					<li><span class="m-box__icon <?php echo get_icon($current_data[14]['icon'], $arr_icons); ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo get_temperature_plus($current_data[14]['temperature']); ?></span></li>
					<li><?php echo round($current_data[14]['windSpeed']); ?> м/с, <?php echo get_wind_direction($current_data[14]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($current_data[4]['pressure']); ?> мм</li>
					<li><?php echo absint($current_data[14]['humidity']*100); ?>%</li>
				</ul>
			</div>
			<div class="col fs-12">
				<ul class="list-unstyled">
					<li class="fs-14">Вечір</li>					
					<li><span class="m-box__icon <?php echo get_icon($current_data[20]['icon'], $arr_icons); ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo get_temperature_plus($current_data[20]['temperature']); ?></span></li>
					<li><?php echo round($current_data[20]['windSpeed']); ?> м/с, <?php echo get_wind_direction($current_data[20]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($current_data[20]['pressure']); ?> мм</li>
					<li><?php echo absint($current_data[20]['humidity']*100); ?>%</li>
				</ul>
			</div>
		</div>
	</section><!--.forecast/-->
	<section class="forecast-mini" style="display:none;">
		<div class="row no-gutters">
			<div class="col-4 pt-2 pb-2 ml-2 fs-14 <?php echo get_weekend_color(time()); ?>">
				<span class="">Сьогодні<br>
				<?php echo (int)date('d');?> 
				<?php echo $month[(int)date('n')];?><br>
				<?php echo $week_short[(int)date('w')];?></span>
			</div>
			<div class="col i-mini-2 ">
				<i class="m-box__icon <?php echo get_icon($current_data[3]['icon'], $arr_icons); ?> va-b"></i>
				<span class="degree"><?php echo get_temperature_plus($current_data[3]['temperature']); ?>&deg;</span>
			</div>
			<div class="col i-mini-2 ">
				<i class="m-box__icon <?php echo get_icon($current_data[14]['icon'], $arr_icons); ?> va-b"></i>
				<span class="degree"><?php echo get_temperature_plus($current_data[14]['temperature']); ?>&deg;</span> 
			</div>
			<div class="col-1 text-right ">
				<i class="m-box__icon-open m-box__icon-open_no-pos ml-0 mr-2 mt-1-75"></i>
			</div>
		</div>
	</section><!--.forecast-mini/-->
</article><!--end today/-->

<?php 
	//
	$time_next_day = absint($time_next_day+3);
	for ($i = $time_next_day, $j = 0; $j < 6; $i+=24 ) { 
?>
	
<article>
	<section class="forecast" style="<?php if($j >= 3) {echo 'display:none';}?>">	
		<div class="row pl-2 no-gutters">
			<div class="col pt-2 fs-14 <?php echo get_weekend_color($hourly_data[$i]['time']); ?>">
				<span class=""><?php if ($j == 0) {echo 'Завтра';} ?> 
				<?php echo (int)date('d', $hourly_data[$i]['time']);?> 
				<?php echo $month[(int)date('n', $hourly_data[$i]['time'])];?><br>
				<?php echo $week[(int)date('w', $hourly_data[$i]['time'])];?></span>
			</div>
			<div class="col text-right pt-2 " >
				<ul class="list-inline" style="margin-bottom:-1rem;margin-left: -20px !important;">
				  <li class="list-inline-item">
					  <i class="m-box__icon <?php echo get_moon_fase($darksky['daily']['data'][$j]['moonPhase']); ?> mr-0 "  style="margin-bottom:-2px !important;"></i></li>
				  <li class="list-inline-item mr-0 mt-1 text-left fs-12"><span>Схід: <?php echo date('H:i', absint($darksky['daily']['data'][$j]['sunriseTime']));?><br>
				  Захід: <?php echo date('H:i', absint($darksky['daily']['data'][$j]['sunsetTime']));?></span></li>
				  <li class="list-inline-item "><i class="m-box__icon-open m-box__icon-open_no-pos m-box__icon-open_up mr-2 ml-0" style="margin-top:-1rem;"></i></li>
				</ul>
			</div>
		</div><!--.row/-->
		<hr style="margin-bottom:0.5rem;margin-top:0.5rem;border-top-color:#cacaca;">
		<div class="row text-center no-gutters">
			<div class="col fs-12">
				<ul class="list-unstyled">					
					<li class="fs-14">Ніч</li>
					<?php $time_index = $i; ?>
					<?php $night_temperature = get_temperature_plus($hourly_data[$time_index]['temperature']); ?>
					<?php $night_icon = get_icon($hourly_data[$time_index]['icon'], $arr_icons); ?>
					<?php $night_summary = $hourly_data[$time_index]['summary']; ?>
					<li><span class="m-box__icon <?php echo $night_icon ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo $night_temperature; ?></span></li>
					<li><?php echo round($hourly_data[$time_index]['windSpeed']); ?> м/с, <?php echo get_wind_direction($hourly_data[$time_index]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($hourly_data[$time_index]['pressure']); ?> мм</li>
					<li><?php echo absint($hourly_data[$time_index]['humidity']*100); ?>%</li>
				</ul>
			</div>
			<div class="col fs-12">
				<ul class="list-unstyled">					
					<li class="fs-14">Ранок</li>
					<?php $time_index += 6; ?>
					<li><span class="m-box__icon <?php echo get_icon($hourly_data[$time_index]['icon'], $arr_icons); ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo get_temperature_plus($hourly_data[$time_index]['temperature']); ?></span></li>
					<li><?php echo round($hourly_data[$time_index]['windSpeed']); ?> м/с, <?php echo get_wind_direction($hourly_data[$time_index]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($hourly_data[$time_index]['pressure']); ?> мм</li>
					<li><?php echo absint($hourly_data[$time_index]['humidity']*100); ?>%</li>
				</ul>
			</div>
			<div class="col fs-12">
				<ul class="list-unstyled">					
					<li class="fs-14">День</li>
					<?php $time_index += 6; ?>
					<?php $day_icon = get_icon($hourly_data[$time_index]['icon'], $arr_icons); ?>
					<?php $day_summary = $hourly_data[$time_index]['summary']; ?>
					<?php $day_temperature = get_temperature_plus($hourly_data[$time_index]['temperature']); ?>
					<li><span class="m-box__icon <?php echo $day_icon ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo $day_temperature; ?></span></li>
					<li><?php echo round($hourly_data[$time_index]['windSpeed']); ?> м/с, <?php echo get_wind_direction($hourly_data[$time_index]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($hourly_data[$time_index]['pressure']); ?> мм</li>
					<li><?php echo absint($hourly_data[$time_index]['humidity']*100); ?>%</li>
				</ul>
			</div>
			<div class="col fs-12">
				<ul class="list-unstyled">					
					<li class="fs-14">Вечір</li>
					<?php $time_index += 6; ?>
					<li><span class="m-box__icon <?php echo get_icon($hourly_data[$time_index]['icon'], $arr_icons); ?> mxt-1"></span></li>
					<li><span class="degree"><?php echo get_temperature_plus($hourly_data[$time_index]['temperature']); ?></span></li>
					<li><?php echo round($hourly_data[$time_index]['windSpeed']); ?> м/с, <?php echo get_wind_direction($hourly_data[$time_index]['windBearing']); ?></li>
					<li><?php echo get_pressure_hPa($hourly_data[$time_index]['pressure']); ?> мм</li>
					<li><?php echo absint($hourly_data[$time_index]['humidity']*100); ?>%</li>
				</ul>
			</div>
		</div>
	</section><!--.forecast/-->
	<section class="forecast-mini" style="<?php if($j < 3) {echo 'display:none';}?>">
		<div class="row no-gutters">
			<div class="col-4 pt-2 pb-2 ml-2 fs-14 <?php echo get_weekend_color($hourly_data[$time_index]['time']); ?>">
				<span><?php if ($j == 0) {echo 'Завтра<br>';} ?>
				<?php echo (int)date('d', $hourly_data[$time_index]['time']);?> 
				<?php echo $month[(int)date('n', $hourly_data[$time_index]['time'])];?><br>
				<?php echo $week_short[(int)date('w', $hourly_data[$time_index]['time'])];?></span>
			</div>
			<div class="col <?php echo $min_class = ($j == 0) ? 'i-mini-2':'i-mini'; ?>">
				<i class="m-box__icon <?php echo $night_icon; ?> va-b"></i>
				<span class="degree"><?php echo $night_temperature; ?>&deg;</span> 
			</div>
			<div class="col <?php echo $min_class = ($j == 0) ? 'i-mini-2':'i-mini'; ?>">
				<i class="m-box__icon <?php echo $day_icon; ?> va-b"></i>
				<span class="degree"> <?php echo $day_temperature; ?>&deg;</span>
			</div>			
			<div class="col-1 text-right ">
				<i class="m-box__icon-open m-box__icon-open_no-pos ml-0 mr-2 <?php echo $min_class = ($j == 0) ? 'mt-1-75':'mt-1-25'; ?>"></i>
			</div>
		</div>
	</section><!--.forecast-mini/-->

<?php $j += 1; } //end for ?>
</article>
<hr style="margin-left:-15px;margin-right:-14px;margin-top:0;border-top-color:#CACACA;">
</div>
</div><!--.container/-->
